### devops gitlab url #
1. url   
- http://10.50.27.61:31089/  

2. 조별 접속 계정   
- 1조 : user21 / garage   
- 2조 : user22 / garage   
- 3조 : user23 / garage   



### devops jenkins url 및 계정 정보   

1. jenkins url   
- http://10.50.27.61:32715/

2. 조별 접속 계정   
- 1조 : user21 / garage   
- 2조 : user22 / garage   
- 3조 : user23 / garage   


### npm 접속 정보 (devops nexus)
1. .npmrc   
```javascript
- registry=http://10.50.27.61:31690/repository/npm-group/
```

### 조 이름 및 네임스페이스

###### 1조 스파게티
- namespace : 4sg   
- front(nodejs) : sg-front
- backend(springboot) : sg-service

###### 2조 RM
- namespace : rm
- front(nodejs) : rm-front
- backend-sysetm (springboot) : rm-service
 
###### 3조 COMS
- namespace : coms
- front(nodejs) : coms-front
- vbackend-sysetm (springboot) : coms-service


### 쿼리 생성
10.50.27.61   
root / happy@cloud   
```sql
CREATE DATABASE msadb DEFAULT CHARACTER SET utf8 collate utf8_unicode_ci;   
CREATE USER 'msa'@'%' IDENTIFIED BY 'passw0rd';   
GRANT ALL PRIVILEGES ON msadb.* TO 'msa'@'%';   
````

### 스프링부트 프로퍼티 변경   
 스프링부트의  application.yml 의 아래 항목을 신규로 생성한 mysql 주소, DB, 계정, 비밀번호로 변경할 것   
url: jdbc:mysql://10.50.27.61:32767/msadb   
