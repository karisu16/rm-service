package com.springboot.microservices.mvp.rest;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.springboot.microservices.mvp.dao.ConsultNoteDao;
import com.springboot.microservices.mvp.dao.EmpInfoDao;
import com.springboot.microservices.mvp.dao.ImageFileDao;
import com.springboot.microservices.mvp.dao.VoiceFileDao;
import com.springboot.microservices.mvp.model.ConsultNote;
import com.springboot.microservices.mvp.model.EmpInfo;
import com.springboot.microservices.mvp.model.ImageFile;
import com.springboot.microservices.mvp.model.VoiceFile;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(value="File Upload API")
@RequestMapping(value="/file")
@RestController
public class FileController {
	
	@Autowired
	private ImageFileDao imageFileDao;
	
	@Autowired
	private VoiceFileDao voiceFileDao;
	
	@Autowired
	private ConsultNoteDao consultNoteDao;
	
	@Autowired
	private EmpInfoDao empInfoDao;	
	
	@ApiOperation(value="이미지파일 업로드")
	@RequestMapping(value="/imgfile/upload", method=RequestMethod.POST)
	public ResponseEntity <String > imgFileUpload(
			@RequestParam("consultId") String consultId,
			@RequestParam("imgRegiDate") String imgRegiDate,
			@RequestParam("file") MultipartFile file
		) throws Exception { 
		ImageFile imageFile = new ImageFile();
		
	
		imageFile.setConsultId(consultId);
		imageFile.setImgRegiDate(imgRegiDate);
		imageFile.setImgContent(file.getBytes());
		imageFile.setImgFileName(file.getOriginalFilename());
		imageFile.setContentType(file.getContentType());
		
		log.info("Start db insert");
		int re  = imageFileDao.insertImgFile(imageFile);
		
		ConsultNote consultNote =  new ConsultNote();
		
		// 컨설트 노트 신규 여부 판단
		if(ObjectUtils.isEmpty(consultNoteDao.selectConsultNotebyconsultId(consultId)))
		{	
			// 신규일떄 
	
			
			EmpInfo empInfo  = null;
			empInfo = empInfoDao.selectEmpInfoByEmpNo("2222222");
			consultNote.setRegiEmpNo(empInfo.getEmpNo());
			consultNote.setRegiBranchNo(empInfo.getBranchCd());
			consultNote.setImgYn("y");
			consultNoteDao.insertConsultNote(consultNote);
		}else
		{
		   // 기존 컨설트 노트일떄 
			consultNote = consultNoteDao.selectConsultNotebyconsultId(consultId);
			consultNote.setImgYn("y");
			consultNoteDao.updateConsultNote(consultNote);
		}
		
		log.debug("result :"+ re);
		
		return new ResponseEntity<String> (re+"", HttpStatus.OK);
	}
	
	@ApiOperation(value="이미지 파일 리스트")
	@RequestMapping(path = "/imgfile/imagelist/{consultId}", method = RequestMethod.GET)
	public ResponseEntity<List<ImageFile>> imageList(
			@PathVariable(name="consultId",required = true ) String consultId
		) throws IOException {
		
		
		ImageFile params = new ImageFile();
		params.setConsultId(consultId);
		List<ImageFile> re = null;
		try {
			log.info("Start db select");
			re = imageFileDao.selectImgFileListbyId(params);

		} catch (Exception e) {
			e.printStackTrace();
		}


		
		return new ResponseEntity<List<ImageFile>> (re, HttpStatus.OK);
	}
	
	@ApiOperation(value="이미지 파일 다운로드")
	@RequestMapping(path = "/imgfile/download/{consultId}/{FileName}", method = RequestMethod.GET)
	public ResponseEntity<Resource> imgFileDownload(
			@PathVariable(name="consultId",required = true ) String consultId,
			@PathVariable(name="FileName",required = true ) String imgFileName
		) throws IOException {
		
		
		ImageFile params = new ImageFile();
		params.setConsultId(consultId);
		params.setImgFileName(imgFileName);
		ImageFile re = null;
		try {
			log.info("Start db select");
			re = imageFileDao.selectImgFile(params);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");
		headers.add("Content-disposition", "attachment; filename=\"" + re.getImgFileName() + "\"");
//		Path path = Paths.get(file.getAbsolutePath());
		ByteArrayResource resource = new ByteArrayResource(re.getImgContent());

		
		return ResponseEntity
				.ok()
				.headers(headers)
//				.contentLength(file.length())
				//.contentType(MediaType.parseMediaType("application/octet-stream"))
				.contentType(MediaType.parseMediaType(re.getContentType()))
				.body(resource);
	}
	
	
	@RequestMapping(value="/imgfile/delete", method=RequestMethod.PUT)
	@ApiOperation(value = "특정 이미지 삭제 consultId FileName 필요")
	public String deleteImageFile(
			@RequestBody ImageFile imageFile
			) {
	
		log.debug("consultId : "+ imageFile.getConsultId());
		
		try {
			
			imageFileDao.deleteImgFile(imageFile);
			
			
			ConsultNote consultNote = consultNoteDao.selectConsultNotebyconsultId(imageFile.getConsultId());
			consultNote.setConsultId(imageFile.getConsultId());
			// 첨부파일이 더있는지 판단
			if(1 <= imageFileDao.checkFileYn(imageFile.getConsultId()))
			{	
				
				consultNote.setImgYn("y");
				consultNoteDao.updateConsultNote(consultNote);
			}else
			{
				consultNote.setImgYn("n");
				consultNoteDao.updateConsultNote(consultNote);
			}
			return "{\"returnCode\":\"SUCCESS\"}";
			
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	
	
	@ApiOperation(value="음성파일 업로드") 
	@RequestMapping(value="/voifile/upload", method=RequestMethod.POST)
	public ResponseEntity <String > voiFileUpload(
			@RequestParam("consultId") String consultId,
			@RequestParam("voiRegiDate") String voiRegiDate,
			@RequestParam("file") MultipartFile file
		) throws Exception { 
		VoiceFile voiceFile = new VoiceFile();
		
	
		voiceFile.setConsultId(consultId);
		voiceFile.setVoiRegiDate(voiRegiDate);
		voiceFile.setVoiContent(file.getBytes());
		voiceFile.setVoiFileName(file.getOriginalFilename());
		voiceFile.setContentType(file.getContentType());
		
		log.info("Start db insert");
		int re  = voiceFileDao.insertVoiFile(voiceFile);
		ConsultNote consultNote = new ConsultNote();
		consultNote.setConsultId(consultId);
		
		// 컨설트 노트 신규 여부 판단
		if(ObjectUtils.isEmpty(consultNoteDao.selectConsultNotebyconsultId(consultId)))
		{	
			// 신규일떄 
			EmpInfo empInfo  = null;
			empInfo = empInfoDao.selectEmpInfoByEmpNo("2222222");
			consultNote.setRegiEmpNo(empInfo.getEmpNo());
			consultNote.setRegiBranchNo(empInfo.getBranchCd());
			consultNote.setImgYn("y");
			consultNoteDao.insertConsultNote(consultNote);
		}else
		{
		   // 기존 컨설트 노트일떄 
			consultNote = consultNoteDao.selectConsultNotebyconsultId(consultId);
			consultNote.setVoiceYn("y");
			consultNoteDao.updateConsultNote(consultNote);
		}
		log.debug("result :"+ re);
		
		return new ResponseEntity<String> (re+"", HttpStatus.OK);
	}
	
	@RequestMapping(path = "/voifile/voicelist/{consultId}", method = RequestMethod.GET)
	public ResponseEntity<List<VoiceFile>> voiceList(
			@PathVariable(name="consultId",required = true ) String consultId
		) throws IOException {
		
		
		VoiceFile params = new VoiceFile();
		params.setConsultId(consultId);
		List<VoiceFile> re = null;
		try {
			log.info("Start db select");
			re = voiceFileDao.selectVoiFileListbyId(params);

		} catch (Exception e) {
			e.printStackTrace();
		}


		
		return new ResponseEntity<List<VoiceFile>> (re, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/voifile/download/{consultId}/{FileName}", method = RequestMethod.GET)
	public ResponseEntity<Resource> voiFileDownload(
			@PathVariable(name="consultId",required = true ) String consultId,
			@PathVariable(name="FileName",required = true ) String voiFileName
		) throws IOException {
		
		
		VoiceFile params = new VoiceFile();
		params.setConsultId(consultId);
		params.setVoiFileName(voiFileName);
		VoiceFile re = null;
		try {
			log.info("Start db select");
			re = voiceFileDao.selectVoiFile(params);

		} catch (Exception e) {
			e.printStackTrace();
		}

		HttpHeaders headers = new HttpHeaders();
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");
		headers.add("Content-disposition", "attachment; filename=\"" + re.getVoiFileName() + "\"");
//		Path path = Paths.get(file.getAbsolutePath());
		ByteArrayResource resource = new ByteArrayResource(re.getVoiContent());

		
		return ResponseEntity
				.ok()
				.headers(headers)
//				.contentLength(file.length())
				//.contentType(MediaType.parseMediaType("application/octet-stream"))
				.contentType(MediaType.parseMediaType(re.getContentType()))
				.body(resource);
	}
	
	@RequestMapping(value="/voifile/delete", method=RequestMethod.PUT)
	@ApiOperation(value = "특정 이미지 삭제 consultId FileName 필요")
	public String deleteVoiceFile(
			@RequestBody VoiceFile voiceFile
			) {
	
		log.debug("consultId : "+ voiceFile.getConsultId());
		
		try {
			
			voiceFileDao.deleteVoiFile(voiceFile);
			ConsultNote consultNote = consultNoteDao.selectConsultNotebyconsultId(voiceFile.getConsultId());
			consultNote.setConsultId(voiceFile.getConsultId());
			// 첨부파일이 더있는지 판단
			if(1 <= voiceFileDao.checkFileYn(voiceFile.getConsultId()))
			{	
				
				consultNote.setVoiceYn("y");
				consultNoteDao.updateConsultNote(consultNote);
			}else
			{
				consultNote.setVoiceYn("n");
				consultNoteDao.updateConsultNote(consultNote);
			}
			
			return "{\"returnCode\":\"SUCCESS\"}";
			
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
}
