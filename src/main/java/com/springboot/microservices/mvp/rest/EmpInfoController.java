package com.springboot.microservices.mvp.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.microservices.mvp.dao.EmpInfoDao;
import com.springboot.microservices.mvp.model.ConsultNote;
import com.springboot.microservices.mvp.model.EmpInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(value="ConsultNote Service API")
@RestController
public class EmpInfoController {

	@Autowired
	private EmpInfoDao empInfoDao;
	
	@ApiOperation(value="사용자 정보 가져오기 ")
	@RequestMapping(value="/empinfo", method=RequestMethod.GET)
	public ResponseEntity <List<EmpInfo>> getEmpInfoList() { 
		
		List<EmpInfo> list = null;
		try {
			log.info("Start db select");
			list = empInfoDao.selectEmpInfoAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("user counts :"+list.size());
		
		return new ResponseEntity<List<EmpInfo>> (list, HttpStatus.OK);
	}
	
	@ApiOperation(value="아이디로 사용자 정보 가져오기 ")
	@RequestMapping(value="/empinfo/{empNo}", method=RequestMethod.GET)
	public ResponseEntity <EmpInfo> getEmpInfoByNo(
				@PathVariable (name="empNo", required = true) String empNo
			) { 
		EmpInfo re = null;
		try {
			log.info("Start db select");
			re = empInfoDao.selectEmpInfoByEmpNo(empNo);
			
			// rabbitmq 
//			broadcastMessageProducer.produceChargeOrder(re);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<EmpInfo> (re, HttpStatus.OK);
	}
}
