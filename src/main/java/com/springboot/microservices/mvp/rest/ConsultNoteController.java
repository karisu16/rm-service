package com.springboot.microservices.mvp.rest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.microservices.mvp.dao.ConsultNoteDao;
import com.springboot.microservices.mvp.dao.EmpInfoDao;
import com.springboot.microservices.mvp.dao.IdGeneratorDao;
import com.springboot.microservices.mvp.dao.ImageFileDao;
import com.springboot.microservices.mvp.dao.SurveyChkDao;
import com.springboot.microservices.mvp.dao.VoiceFileDao;
import com.springboot.microservices.mvp.model.ConsultNote;
import com.springboot.microservices.mvp.model.ConsultNoteView;
import com.springboot.microservices.mvp.model.EmpInfo;
import com.springboot.microservices.mvp.model.ImageFile;
import com.springboot.microservices.mvp.model.VoiceFile;
import com.springboot.microservices.mvp.model.IdGenerator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(value="ConsultNote Service API")
@RestController
public class ConsultNoteController {
	
	@Autowired
	private ConsultNoteDao consultNoteDao;
	@Autowired
	private EmpInfoDao empInfoDao;	
	@Autowired
	private ImageFileDao imageFileDao;
	@Autowired
	private IdGeneratorDao idGeneratorDao;
	
	@Autowired
	private VoiceFileDao voiceFileDao;
	
	@Autowired
	private SurveyChkDao surveyChkDao;
	
	
	
	@ApiOperation(value="상담일지 상세 정보 요청 ")
	@RequestMapping(value="/consult/getConsultNoteViewbyConsultId/{consultId}", method=RequestMethod.GET)
	public ResponseEntity <ConsultNoteView> getConsultNoteViewbyConsultId(
			@PathVariable(name="consultId",required = true ) String consultId
			) { 
		
		ConsultNoteView re = null;
		
	
		try {
			ConsultNoteView consultNoteView = new ConsultNoteView();
			ConsultNote consultNote = new ConsultNote();
			EmpInfo empInfo  = null;
			 empInfo = empInfoDao.selectEmpInfoByEmpNo("2222222");
			consultNote.setRegiEmpNo(empInfo.getEmpNo());
			consultNote.setRegiBranchNo(empInfo.getBranchCd());
			//log.info("Start db select");
//			re = consultNoteDao.selectConsultNoteListbyImportantY(consultNote);
			
			// 컨설트 노트 담기 
			consultNoteView.setConsultnote(consultNoteDao.selectConsultNotebyconsultId(consultId));
			
			
			// 이미지 첨부 여부 체크 및 이미지 리스트 담기
			if(consultNoteView.getConsultnote().getImgYn().equals(StringUtils.trimAllWhitespace("y")))
			{
				ImageFile imgParams = new ImageFile();
				imgParams.setConsultId(consultId);
				consultNoteView.setImageFiles(imageFileDao.selectImgFileListbyId(imgParams));
			}
			// 이미지 첨부 여부 체크 및 이미지 리스트 담기
			if(consultNoteView.getConsultnote().getVoiceYn().equals(StringUtils.trimAllWhitespace("y")))
			{
				VoiceFile voiParams = new VoiceFile();
				voiParams.setConsultId(consultId);
				consultNoteView.setVoiceFiles(voiceFileDao.selectVoiFileListbyId(voiParams));
			}
			
			// 체크리스트 리스트 담기
			consultNoteView.setSurveyChk(surveyChkDao.selectByConsultId(consultId));
			
			re = consultNoteView;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ResponseEntity<ConsultNoteView> (re, HttpStatus.OK);
	}
	
	@ApiOperation(value="중요상담일지 리스트 가져오기 ")
	@RequestMapping(value="/consult/selectConsultNoteListbyimportant", method=RequestMethod.GET)
	public ResponseEntity <List<ConsultNote>> getConsultNoteListbyimportantSearch() { 
		
		List<ConsultNote> re = null;
		
	
		try {
			ConsultNote consultNote = new ConsultNote();
			EmpInfo empInfo  = null;
			 empInfo = empInfoDao.selectEmpInfoByEmpNo("2222222");
			consultNote.setRegiEmpNo(empInfo.getEmpNo());
			consultNote.setRegiBranchNo(empInfo.getBranchCd());
			//log.info("Start db select");
			re = consultNoteDao.selectConsultNoteListbyImportantY(consultNote);
			
			// rabbitmq 
//			broadcastMessageProducer.produceChargeOrder(re);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ResponseEntity<List<ConsultNote>> (re, HttpStatus.OK);
	}
	
	@ApiOperation(value="상담일지 Id 채번")
	@RequestMapping(value="/consult/getConsultId", method=RequestMethod.GET)
	public ResponseEntity <IdGenerator> getConsultId() { 
		
		IdGenerator re = null;
		
	
		try {
			Integer id = idGeneratorDao.CreateId();
				
			re = idGeneratorDao.selectConsultId();
			
			// rabbitmq 
//			broadcastMessageProducer.produceChargeOrder(re);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ResponseEntity<IdGenerator> (re, HttpStatus.OK);
	}
	
	@ApiOperation(value="검색조건으로 리스트 가져오기 ")
	@RequestMapping(value="/consult/consultnotelistsearch", method=RequestMethod.POST)
	public ResponseEntity  <List<ConsultNote>> getConsultNoteListSearch(
				@RequestBody ConsultNote consultNote
			) { 
		
		List<ConsultNote> re = null;
		
	
		try {
			EmpInfo empInfo  = null;
			 empInfo = empInfoDao.selectEmpInfoByEmpNo("2222222");
			consultNote.setRegiEmpNo(empInfo.getEmpNo());
			consultNote.setRegiBranchNo(empInfo.getBranchCd());
			//log.info("Start db select");
			re = consultNoteDao.selectConsultNoteList(consultNote);
			
			// rabbitmq 
//			broadcastMessageProducer.produceChargeOrder(re);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ResponseEntity<List<ConsultNote>> (re, HttpStatus.OK);
	}
	
	@ApiOperation(value="상담일지 테이블 등록")
	@RequestMapping(value="/consult/insertConsultNote", method=RequestMethod.PUT)
	public String insertConsultNote(
				@RequestBody ConsultNote consultNote
			) { 
		
	
		try {
			//로그인 대체 코드 사용자 정보 받기 
			EmpInfo empInfo  = null;
			empInfo = empInfoDao.selectEmpInfoByEmpNo("2222222");
			consultNote.setRegiEmpNo(empInfo.getEmpNo());
			consultNote.setRegiBranchNo(empInfo.getBranchCd());
			
								
			consultNoteDao.insertConsultNote(consultNote);
			
			
			//log.info("Start db select");
		//	re = consultNoteDao.selectConsultNoteList(consultNote);
			
			// rabbitmq 
//			broadcastMessageProducer.produceChargeOrder(re);
		} catch (Exception e) {
			return e.getMessage();
		}
		
		return "{\"returnCode\":\"SUCCESS\"}";
	}
	
	@RequestMapping(value="/consult/editConsultNote", method=RequestMethod.PUT)
	@ApiOperation(value = "상담일지 편집 수정")
	public String editConsultNote(
			@RequestBody ConsultNote consultNote
			) {

		log.debug("consultId : "+ consultNote.getConsultId());
		
		try {
			EmpInfo empInfo  = null;
			empInfo = empInfoDao.selectEmpInfoByEmpNo("2222222");
			consultNote.setRegiEmpNo(empInfo.getEmpNo());
			consultNote.setRegiBranchNo(empInfo.getBranchCd());
			
			consultNoteDao.updateConsultNote(consultNote);
			return "{\"returnCode\":\"SUCCESS\"}";
			
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	@RequestMapping(value="/consult/deletConsultNote", method=RequestMethod.PUT)
	@ApiOperation(value = "상담일지 삭제")
	public String deletConsultNote(
			@RequestBody ConsultNote consultNote
			) {
	
		log.debug("consultId : "+ consultNote.getConsultId());
		
		try {
			
			
			// 사진 삭제 
			if(1 <= imageFileDao.checkFileYn(consultNote.getConsultId()))
			{
				ImageFile imgParams = new ImageFile();
				imgParams.setConsultId(consultNote.getConsultId());
				imageFileDao.deleteImgFileList(imgParams);
			}
			// 음성 삭제
			if(1 <= voiceFileDao.checkFileYn(consultNote.getConsultId()))
			{
				VoiceFile voiParams = new VoiceFile();
				voiParams.setConsultId(consultNote.getConsultId());
				voiceFileDao.deleteVoiFileList(voiParams);
			}
			// 설문 삭제 
			if(1 <= surveyChkDao.checkSurveyYn(consultNote.getConsultId()))
			{
				surveyChkDao.deleteSurveyChkList(consultNote.getConsultId());
			}
			
			
			// 컨설트 노트 삭제 			
			consultNoteDao.deletConsultNote(consultNote);
			
			return "{\"returnCode\":\"SUCCESS\"}";
			
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	
	@RequestMapping(value="/consult/consultnoteupdate/{consultId}/{importantYn}", method=RequestMethod.PUT)
	@ApiOperation(value = "중요 상담일지 Y/N 변경")
	public String updateConsultNoteImportant(
				@PathVariable(name="consultId",required = true ) String consultId,
				@PathVariable(name="importantYn",required = true ) String importantYn
			) {
	
		log.debug("consultId : "+ consultId);
		try {
			ConsultNote consultNote = new ConsultNote();
			consultNote.setConsultId(consultId);
			consultNote.setImportantYn(importantYn);
	
			consultNoteDao.updateConsultNoteImportant(consultNote);
			return "{\"returnCode\":\"SUCCESS\"}";
			
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	
}
