package com.springboot.microservices.mvp.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.microservices.mvp.dao.SurveyChkDao;
import com.springboot.microservices.mvp.model.ImageFile;
import com.springboot.microservices.mvp.model.SurveyChk;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

	@Slf4j
	@Api(value = "surveyChk Service API")
	@RestController
	public class SurveyChkController {

		@Autowired
		private SurveyChkDao surveyChkDao;

		@ApiOperation(value = "사용자 정보 가져오기 ")
		@RequestMapping(value = "/surveyChk", method = RequestMethod.GET)
		public ResponseEntity<List<SurveyChk>> getSurveyChkList() {

			List<SurveyChk> list = null;
			try {
				log.info("Start db select");
				list = surveyChkDao.selectSurveyChkAll();
			} catch (Exception e) {
				e.printStackTrace();
			}
			log.debug("user counts :" + list.size());

			return new ResponseEntity<List<SurveyChk>>(list, HttpStatus.OK);
		}

		@ApiOperation(value = "아이디로 사용자 정보 가져오기 ")
		@RequestMapping(value = "/surveyChk/{consultId}", method = RequestMethod.GET)
		public ResponseEntity<List<SurveyChk>> getSurveyChkByNo(
				@PathVariable(name = "consultId", required = true) String consultId) {
			List<SurveyChk> re = null;
			try {
				log.info("Start db select");
				re = surveyChkDao.selectByConsultId(consultId);

				// rabbitmq
//				broadcastMessageProducer.produceChargeOrder(re);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return new ResponseEntity<List<SurveyChk>>(re, HttpStatus.OK);
		}
		
		
		@RequestMapping(value="/surveychk/delete", method=RequestMethod.PUT)
		@ApiOperation(value = "체크 해제 consultId suvey_cd 필요")
		public String deleteSurveyChk(
				@RequestBody SurveyChk surveyChk
				) {
		
			log.debug("consultId : "+ surveyChk.getConsultId());
			
			try {
				
				surveyChkDao.deleteSurveyChk(surveyChk);
				return "{\"returnCode\":\"SUCCESS\"}";
				
			} catch (Exception e) {
				return e.getMessage();
			}
		}
		
		@RequestMapping(value="/surveychk/insert", method=RequestMethod.PUT)
		@ApiOperation(value = "체크 지정 consultId suvey_cd 필요")
		public String insertSurveyChk(
				@RequestBody SurveyChk surveyChk
				) {
		
			log.debug("consultId : "+ surveyChk.getConsultId());
			
			try {
				
				surveyChkDao.insertSurveyChk(surveyChk);
				return "{\"returnCode\":\"SUCCESS\"}";
				
			} catch (Exception e) {
				return e.getMessage();
			}
		}
	}
