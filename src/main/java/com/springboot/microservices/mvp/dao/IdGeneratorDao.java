package com.springboot.microservices.mvp.dao;

import org.apache.ibatis.annotations.Mapper;

import com.springboot.microservices.mvp.model.IdGenerator;

@Mapper
public interface IdGeneratorDao {
		
	Integer CreateId() throws Exception;
	
	IdGenerator selectConsultId() throws Exception;
	
	
}
