package com.springboot.microservices.mvp.dao;

import com.springboot.microservices.mvp.model.ImageFile;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ImageFileDao {

	int insertImgFile(ImageFile imageFile) throws Exception;
	
	ImageFile selectImgFile(ImageFile imageFile) throws Exception;	
	
	List<ImageFile> selectImgFileListbyId(ImageFile imageFile) throws Exception;	
	
	int deleteImgFileList(ImageFile imageFile) throws Exception;
	
	int deleteImgFile(ImageFile imageFile) throws Exception;
	
	int checkFileYn(String consultId)throws Exception;
}
