package com.springboot.microservices.mvp.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


import com.springboot.microservices.mvp.model.EmpInfo;

@Mapper
public interface EmpInfoDao {

	

	List<EmpInfo> selectEmpInfoAll() throws Exception;
	
	/**
	 * 아이디로 사용자 정보 조회하
	 * @param regEmpNo
	 * @return
	 * @throws Exception
	 */
	EmpInfo selectEmpInfoByEmpNo(String empNo) throws Exception;
}
