package com.springboot.microservices.mvp.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.springboot.microservices.mvp.model.ConsultNote;


@Mapper
public interface ConsultNoteDao {

	List<ConsultNote> selectConsultNoteAll() throws Exception;
	
	ConsultNote selectConsultNotebyconsultId(String consultId) throws Exception;
	
	List<ConsultNote> selectConsultNoteListbyImportantY(ConsultNote consultNote) throws Exception;	
	
	List<ConsultNote> selectConsultNoteList(ConsultNote consultNote) throws Exception;	
	
	int updateConsultNoteImportant(ConsultNote consultNote) throws Exception;
	
	int updateConsultNote(ConsultNote consultNote) throws Exception;	
	
	int insertConsultNote(ConsultNote consultNote) throws Exception;
	
	int deletConsultNote(ConsultNote consultNote) throws Exception;	
	
	int checkConsultNote(String consultId) throws Exception;
}
