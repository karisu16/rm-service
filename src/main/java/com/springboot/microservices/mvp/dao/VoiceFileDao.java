package com.springboot.microservices.mvp.dao;

import com.springboot.microservices.mvp.model.VoiceFile;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface VoiceFileDao {

	int insertVoiFile(VoiceFile voiceFile) throws Exception;
	
	VoiceFile selectVoiFile(VoiceFile voiceFile) throws Exception;	
	
	List<VoiceFile> selectVoiFileListbyId(VoiceFile voiceFile) throws Exception;	
	
	int deleteVoiFileList(VoiceFile voiceFile) throws Exception;
	
	int deleteVoiFile(VoiceFile voiceFile) throws Exception;
	
	int checkFileYn(String consultId)throws Exception;
}
