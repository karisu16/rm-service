package com.springboot.microservices.mvp.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


import com.springboot.microservices.mvp.model.SurveyChk;

@Mapper
public interface SurveyChkDao {


	List<SurveyChk> selectSurveyChkAll() throws Exception;
	
	/**
	 * 아이디로 사용자 정보 조회하
	 * @param regEmpNo
	 * @return
	 * @throws Exception
	 */
	List<SurveyChk> selectByConsultId(String consultId) throws Exception;
	
	int insertSurveyChk(SurveyChk surveyChk) throws Exception;
	
	int deleteSurveyChk(SurveyChk surveyChk) throws Exception;
	
	int deleteSurveyChkList(String consultId) throws Exception;
	
	int checkSurveyYn(String consultId) throws Exception;
}
