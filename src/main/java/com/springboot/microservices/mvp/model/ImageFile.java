package com.springboot.microservices.mvp.model;

import lombok.Data;

@Data
public class ImageFile {

	 private String  consultId		; // 상담일지 ID
	 private String  imgRegiDate	; // 이미지등록일자
	 
	 // file info
	 private String contentType;  // 파일 타입
	 private String imgFileName;  // 이미지파일명
	 private byte[] imgContent;   // 이미지 콘텐츠
}
