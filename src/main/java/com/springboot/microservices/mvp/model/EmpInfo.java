package com.springboot.microservices.mvp.model;


	import lombok.Data;

	@Data
	public class EmpInfo {
		private	String	empNo;	//	직번
		private	String	empNm;	//	직원명
		private	String	empPosition	;	//	직명
		private	String	beforeBranchCd;	//	구 영업점 번호
		private	String	beforeBranchNm;	//	구 영업점 이름
		private	String	branchCd;	//	현 영업점 번호
		private	String	branchNm;	//	현 영업점 이름
		private	String	empCall;	//	연락처
		private	String	empEmail;	//	이메일

	}

