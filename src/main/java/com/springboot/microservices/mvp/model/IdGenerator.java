package com.springboot.microservices.mvp.model;

import lombok.Data;

@Data
public class IdGenerator {
	private	String	seqCurrval;	//	시퀀스 
	private	String	seqDate;	//	오늘날짜 
	private	String	consultId;  // 컨설트 ID
}
