package com.springboot.microservices.mvp.model;

import lombok.Data;


@Data
public class ConsultNote {

	private	String	consultId	;	//상담일지ID
	private	String	regiDate	;	//등록일자
	private	String	updateDate	;	//수정일자
	private	String	regiEmpNo	;	//작성직원번호
	private	String	EmpNm	;	//작성직원이름 
	private	String	updateEmpNo	;	//변경직원번호
	private	String	textYn	;	//텍스트저장유무
	private	String	imgYn	;	//이미지저장유무
	private	String	voiceYn	;	//음성저장유무
	private	String	copCustId	;	//기업고객담당자번호
	private	String	copCustNm	;	//기업고객담당자이름
	private	String	copCustPhone	;	//기업고객담당자폰번호
	private	String	copCustPosition	;	//기업고객담당자직책
	private	String	copCustEmail	;	// emall
	private	String	copNo	;	//기업사업자번호
	private	String	copNm	;	//기업사업자이름
	private	String	importantYn	;	//중요상담일지여부
	private	String	regiBranchNo	;	//등록 지점번호
	private	String	BranchNm	;	//등록 지점이름  
	private	String	openLevel	;	//공개범위
	private	String	textContent	;	//텍스트저장 내용

}
