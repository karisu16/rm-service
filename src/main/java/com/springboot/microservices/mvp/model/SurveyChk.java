package com.springboot.microservices.mvp.model;
import lombok.Data;

@Data
public class SurveyChk {
	private	String	consultId	;	//상담일지ID
	private	String	surveyCd	;	//설문코드
	private String  serveyNm	;  // 설문코드이름
}
