package com.springboot.microservices.mvp.model;



import java.util.List;

import lombok.Data;

@Data
public class ConsultNoteView {
	
	private ConsultNote consultnote ; // 컨설트 노트 헤더
	private List<ImageFile> imageFiles ; // 첨부 이미지 리스트 파일
	private List<VoiceFile> voiceFiles ; // 첨부 음성 리스트 파일
	private List<SurveyChk> surveyChk ; // 설문형 작성 체크 리스트 
 	
}
